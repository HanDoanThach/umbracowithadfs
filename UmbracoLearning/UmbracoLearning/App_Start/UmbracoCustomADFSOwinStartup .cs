﻿using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.WsFederation;
using Owin;
using System.Configuration;
using System.Linq;
using Umbraco.Core.Models.Identity;
using Umbraco.Core.Security;
using Umbraco.Web;
using Umbraco.Web.Security;
using Microsoft.Owin.Security.Cookies;
using UmbracoLearning;

//To use this startup class, change the appSetting value in the web.config called 
// "owin:appStartup" to be "UmbracoCustomOwinStartup"

[assembly: OwinStartup("UmbracoCustomADFSOwinStartup", typeof(UmbracoCustomADFSOwinStartup))]

namespace UmbracoLearning
{
    public class UmbracoCustomADFSOwinStartup : UmbracoDefaultOwinStartup
    {
        public override void Configuration(IAppBuilder app)
        {
            //Configure the Identity user manager for use with Umbraco Back office

            // *** EXPERT: There are several overloads of this method that allow you to specify a custom UserStore or even a custom UserManager!            
            app.ConfigureUserManagerForUmbracoBackOffice(
               Services,
               Mapper,
               UmbracoSettings.Content,
               GlobalSettings,
               //The Umbraco membership provider needs to be specified in order to maintain backwards compatibility with the 
               // user password formats. The membership provider is not used for authentication, if you require custom logic
               // to validate the username/password against an external data source you can create create a custom UserManager
               // and override CheckPasswordAsync
               global::Umbraco.Core.Security.MembershipProviderExtensions.GetUsersMembershipProvider().AsUmbracoMembershipProvider());


            // Due to a problem with the cookie management caused by OWIN and ASP.NET we need to use this KentorOwinCookieSaver as described here: https://stackoverflow.com/a/26978166/465509
            // If this isn't used, the external login will seemingly randomly stop working after a while.
            app.UseKentorOwinCookieSaver();

            //Ensure owin is configured for Umbraco back office authentication
            base.Configuration(app);

            // Configure additional back office authentication options            
            ConfigureBackOfficeAdfsAuthentication(app);
        }

        private static void ConfigureBackOfficeAdfsAuthentication(
            IAppBuilder app,
            string caption = "AD FS",
            string style = "btn-microsoft",
            string icon = "fa-windows")
        {
            // Load configuration from web.config
            var adfsMetadataEndpoint = ConfigurationManager.AppSettings["AdfsMetadataEndpoint"];
            var adfsRelyingParty = ConfigurationManager.AppSettings["AdfsRelyingParty"];
            var adfsFederationServerIdentifier = ConfigurationManager.AppSettings["AdfsFederationServerIdentifier"];
            var adfsReplyUrl = ConfigurationManager.AppSettings["AdfsReplyUrl"];

            app.SetDefaultSignInAsAuthenticationType(Umbraco.Core.Constants.Security.BackOfficeExternalAuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            var wsFedOptions = new WsFederationAuthenticationOptions
            {
                Wtrealm = adfsRelyingParty,
                MetadataAddress = adfsMetadataEndpoint,
                SignInAsAuthenticationType = Umbraco.Core.Constants.Security.BackOfficeExternalAuthenticationType,
                Caption = caption,
                Wreply = adfsRelyingParty, // Redirect to the Umbraco back office after succesful authentication
            };

            //The crucial bit, where we hook into the events when users login or when they are created
            wsFedOptions.SetExternalSignInAutoLinkOptions(new ExternalSignInAutoLinkOptions(true, new string[0])
            {
                OnAutoLinking = OnAutoLinking,
                OnExternalLogin = OnExternalLogin
            });

            // Apply options
            wsFedOptions.ForUmbracoBackOffice(style, icon);
            wsFedOptions.AuthenticationType = adfsFederationServerIdentifier;
            app.UseWsFederationAuthentication(wsFedOptions);
        }

        private static void OnAutoLinking(BackOfficeIdentityUser autoLinkUser, ExternalLoginInfo loginInfo)
        {
            OnExternalLogin(autoLinkUser, loginInfo);
        }

        private static bool OnExternalLogin(BackOfficeIdentityUser autoLinkUser, ExternalLoginInfo loginInfo)
        {
            var defaultRoleADFS = ConfigurationManager.AppSettings["DefaultRoleADFS"];
            SetGroupForUser(autoLinkUser, defaultRoleADFS);
            return true;
        }  

        private static void SetGroupForUser(BackOfficeIdentityUser autoLinkUser, string groupName)
        {
            if (!autoLinkUser.Roles.Any(role => role.RoleId == groupName))
            {
                var userService = Umbraco.Core.Composing.Current.Services.UserService;
                var userGroup = userService.GetUserGroupByAlias(groupName);
                if (userGroup != null)
                {
                    autoLinkUser.AddRole(userGroup.Alias);
                }               
            }
        }
    }
}